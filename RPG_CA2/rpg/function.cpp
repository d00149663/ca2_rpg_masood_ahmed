#include <iostream>
#include <string>
#include "Character.h"
#include "weapon.h"
#include <windows.h>
#include "function.h"

using namespace std;

//startscreen for the main method where the game starts
void startScreen()
{
	system("cls");
	system("color c");//change the text colour
	cout << "********************************************************************************\n";
	cout << "******************             UNTOLD STORY OF HELL        *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300); // beep is the sound beeping first number is the tone hi or low and second number is how long the tone goes
	Beep(150, 200);
	Beep(250, 200);

	system("cls"); // it clears the text in the system
	cout << "********************************************************************************\n";
	cout << "******************        A New Story To be Discovered!     ********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************                 One Dream	            *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************               Real or fake              *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************           An Un-escapeable Dream         ********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	//chapter 2 from where we left from chapter 1
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "******************             Chapter Two                 *********************\n";
	cout << "******************           Combat Training Quest         *********************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	Beep(350, 100);
	Beep(200, 300);
	Beep(300, 200);
	Beep(200, 300);
	Beep(300, 200);

	system("pause");
	system("cls");
}
void gameOver()
{
	//game over function
	system("color e");
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "******************              Game Over                  *********************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	Beep(300, 500);
	Beep(100, 500);
	system("pause");
	system("cls");
}

character chooseClass()
{
	//user input for class they want to be
	int classChoice = 0;
	cout << "What is your playstyle?\n\n";
	cout << "[1] Warrior - balanced/higher damage\n";//warrior class
	cout << "[2] Samurai - Sword Specialist\n";//samurai class
	cout << "[3] Ninja - Who\n";//ninja classs
	
	cout << "\n> ";
	//validate for the choiceclass
	while (!(cin >> classChoice) || classChoice < 1 || classChoice > 3)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	};
	//swictch statement for te choiceclasses with the demagemultiplier from the weapon class
	switch (classChoice)
	{
	case 1:
		cout << "THE WARRIOR IS BORN NOT MADE, HE KNOWS THAT HIS SOUL PURPOSE IN LIFE IS TO SERVE AND FIGHT.\n";
		return character("fighter", 50, 12, .65, 50, 1);//setClass(z), setHealth(a), setStr(b),	setArmor(i), setGold(e), setLevel(f);
		break;
	case 2:
		cout << "Samurai- Skillfull in swords and master with shap weapons.\n";
		return character("Samurai", 45, 26, .96, 55, 4);
		break;
	case 3:
		cout << "Ninja - a fast and deadly creature that hunts and kills its foes\n";
		return character("Ninja", 55, 12, .85, 20, 3);
		break;
	default:
		cout << "You are doing it wrong! Press either '1' or '2', or '3' nothing else!\n";

		cin.ignore();
		cin.get();
		system("cls");
		return chooseClass();
	}
}

weapon chooseWeapon()
{
	cout << "Please choose from the following instruments of war.\n\n";
	cout << "[1]Sword\n";
	cout << "[2]Light Saber\n";
	cout << "[3]Dagger\n";
	cout << "[4]Axe\n";
	cout << "[5]Stick\n\n";
	int weaponChoice = 0;

	cout << "\n> ";
	while (!(cin >> weaponChoice) || weaponChoice < 1 || weaponChoice > 5)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	}
	switch (weaponChoice)
	{
	case 1:
		cout << "Sword, a fine choice but be carefull.";
		return weapon("Sword", 2, 0);//WeaponName(a), DamageMultiplier1(b), DamageMultiplier2(c);
		break;
	case 2:
		cout << "Light Saber, a fine choice rare quality lad.";
		return weapon("Light Saber", 1, 1);
		break;
	case 3:
		cout << "dagger, a fine choice its pointy so mind it there kid.";
		return weapon("dagger", 2, 0);
		break;
	case 4:
		cout << "Axe, fine choices hope its not heavy.";
		return weapon("Axe", 2, 0);
		break;
	case 5:
		cout << "Claymore, a fine choice.";
		return weapon("Stick", 1, 1);
		break;
	default:
		cout << "Invalid class selection.\n\n";
		cout << "Please press enter to continue\n";
		cin.get();
		system("cls");
		return chooseWeapon();
	}
}

