#ifndef WEAPON_H
#define WEAPON_H

class weapon
{
private:
	//sing weapon name and float weapon multiplier
	std::string weaponName;
	float damageMultiplier1;
	float damageMultiplier2;

public:
	//demage multiplier and weapon name is called from .cpp as and reads in the setters from the file and gets the following information
	weapon(std::string a, float b, float c);
	void setWeaponName(std::string x);
	void setDamageMultiplier1(float y);
	void setDamageMultiplier2(float z);
	void upGrade();
	void attachment();
	//now this will be the getters and will get the information needed
	std::string getWeaponName()
	{
		return weaponName;
	}
	float getDamageMultiplier1()
	{
		return damageMultiplier1;
	}
	float getDamageMultiplier2()
	{
		return damageMultiplier2;
	}
};

#endif // WEAPON_H
