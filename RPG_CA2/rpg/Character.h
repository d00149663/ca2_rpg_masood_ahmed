#ifndef CHARACTER_H
#define CHARACTER_H
//player character class
class character
{
private:
	//something that is not changable by players will remain same. like health will have its functions and unchangable by player cept by other means
	//values gain from items will remain same unchainable
	std::string pClass;
	int health;
	int str;
	float armor;
	int gold;
	int level;
public:
	//public class
	character(std::string z, int a, int b, float i, int e, int f);
	//the character classes
	void setClass(std::string x);
	void setHealth(int h);
	void setStr(int s);
	void setArmor(float arm);
	void setGold(int g);
	void setLevel(int l);
	void addHealth();
	void subHealth();
	void addGold();
	void subGold();
	void addLevel();


	//few getters for he character when he gains health level etc
	std::string getClass()//get the player class
	{
		return pClass;
	}
	int getHealth()//get the player health
	{
		return health;
	}
	int getStr()//player strength
	{
		return str;
	}
	float getArmor()//get player armor
	{
		return armor;
	}
	int getGold()//player gold
	{
		return gold;
	}
	int getLevel()//level gains
	{
		return level;
	}


};


#endif // CHARACTER_H
