//getting the getters and setters for the weapon classes
#include <iostream>
#include <string>
#include "weapon.h"//weapon header file

using namespace std;

weapon::weapon(string a, float b, float c)
{
	setWeaponName(a);
	setDamageMultiplier1(b);
	setDamageMultiplier2(c);
}
//few setters to set the weapon name and demagemultiplier into name string and demage as float

void weapon::setWeaponName(string x)
{
	weaponName = x;
}
void weapon::setDamageMultiplier1(float y)
{
	damageMultiplier1 = y;
}
void weapon::setDamageMultiplier2(float z)
{
	damageMultiplier2 = z;
}
void weapon::upGrade()
//demage culcalculator will add demage multiplayer * 0.50 for 1st multiplier and will 0.20 for second
{
	damageMultiplier1 = damageMultiplier1 + 0.50;
}
void weapon::attachment()
{

	damageMultiplier2 = damageMultiplier2 + 0.20;
}
