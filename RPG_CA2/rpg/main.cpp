//Author: Masood Ahmed (D00149663)
//Project: CA2
//Module: Games Programming
//this is the main methid and function for the text-based game
//added main libruaries for the file 
#include <iostream>// standard input/output library for console.
#include <string>// is to handle strings
#include "Character.h"//character.h file
#include "weapon.h"//weapon.h file
#include <windows.h>//used to maximize command screen
#include "function.h"//function.h file
#include <cstdlib>// To be able to use system it is needed as an identifier
#include <ctime>

//using namespace std mainly atm
using namespace std;

//Methods included
character chooseClass();
weapon chooseWeapon();
void startScreen();
void theGame();
void gameOver();

int main()//main function
{
	//tells screen to maximize
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_MAXIMIZE);

	startScreen();
	cout << "Aloha Adventurer what shell be your Last Name\n";
	string name; 
	cout << "\n> ";
	cin >> name;
	cout << "You are " << name << " that is a Strange name \n\n";
	system("pause");

	char player;
	int newGold;
	int newHealth;
	int newStr;
	int experience = 0;
	system("cls");
	character pHero = chooseClass();
	system("pause");
	system("cls");
	weapon pWeapon = chooseWeapon();
	system("pause");
	system("cls");
	cout << "\n> ";


while (pHero.getHealth()>0)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//pHere class hero will gain level acourding to experience earned
	//this while statement is the level up for points required for player hero level ups
	if (experience > 500 && pHero.getLevel() <2)
	{
		cout << "Congrats You are level 2 now!\n\n";
		pHero.addLevel();
		system("pause");
		system("cls");
	}
	if (experience > 1000 && pHero.getLevel() <3)
	{
		cout << "Congrats You are level 3 now!\n\n";
		pHero.addLevel();
		system("pause");
		system("cls");
	}
	if (experience > 1500 && pHero.getLevel() <4)
	{
		cout << "Congrats You are level 4 now!\n\n";
		pHero.addLevel();
		system("pause");
		system("cls");
	}
	if (experience > 2000 && pHero.getLevel() <5)
	{
		cout << "Congrats You are level 5 now!\n\n";
		pHero.addLevel();
		system("pause");
		system("cls");
	}
	if (experience > 3000 && pHero.getLevel() <6)
	{
		cout << "Congrats You are level 6 now!\n\n";
		pHero.addLevel();
		system("pause");
		system("cls");
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	//rooms in the room.txt
		cout << "Please Select Where to go:\n\n";
		cout << "[1]Armoury Store\n";
		cout << "[2]Challenge Hall\n";
		cout << "[3]Items Store\n";
		cout << "[4]Stats\n";
		cout << "[5]Exit\n\n";

		int menuSelect;
		srand(time(0));//gives the random time in seconds 
		while (!(cin >> menuSelect) || menuSelect < 1 || menuSelect > 5)
		{
			cout << "invalid variable. must be in range of asked choice.\n";
			cin.clear();
			cin.ignore(100, '\n');
		}
		//_____________________________________________________________________________________________________________________
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//_____________________________________________________________________________________________________________________
		switch (menuSelect)
		{
		case 1:     // Armory Store
		{	

		system("cls");
			cout << "Armoury Store\n";
			cout << "Current Gold " << pHero.getGold() << " G.\n\n";
			cout << "Upgrade Weapon?\n";
			cout << "[1]Weapon Upgrade  - 50g\n";
			cout << "[2]Weapon Attachment - 25g\n";
			cout << "[3]Exit\n";

			int choice;
			cin >> choice;
			system("cls");
			cout << "\n> ";
			//validate
			while (!(cin >> choice) || choice < 1 || choice > 3)
			{
				cout << "invalid variable. must be in range of asked choice.\n";
				cin.clear();
			}

			switch (choice)
				{
				//choices made to spend gold on the store
				//choces made in the store
			case 1:
				{
					if (pHero.getGold() > 49)
						//if gold more then 49(50) then allow to buy upgrade and substract -50 grom the current gold
				{
					pWeapon.upGrade();//upgrade weapon
					newGold = pHero.getGold() - 50;//substract -50 
					pHero.setGold(newGold);//set new gold value   after weapon upgrades
					cout << "Main-hand Weapon upgraded!";
					system("pause");
					system("cls");
			break;
					}
			else
				//if not enough gold then you are told to comback when have enough
					{
				cout << "This requires more G please comeback after you have enough G";
				system("pause");
				system("cls");
				break;
			}
			}
			case 2:
			{
			 if (pWeapon.getDamageMultiplier2() > 0)
			 {//same as other statement above
				 if (pHero.getGold()>24)
				 {
					pWeapon.attachment();
					newGold = pHero.getGold() - 25;
					pHero.setGold(newGold);
					cout << "You received attachment for your weapon now its stronger!";
					system("pause");
					system("cls");
					break;
				 }
				 else
				 {
					 cout << "This requires more G please comeback after you have enough G";
					 system("pause");
					 system("cls");
					 break;
				 }
			 }
			 else
			 {
				 cout << "Weapon cannot be upgraded";
				 system("pause");
				 system("cls");
				 break;
			 }
			}
			case 3:
			{
			break;
			}
			default:
			{
					   cout << "Invalid selection.\n\n";
					   system("pause");
					   system("cls");
			}
			}
			break;
		}
//_____________________________________________________________________________________________________________________
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//_____________________________________________________________________________________________________________________
		case 2:     // Challenge Hall
		{
						int ChallengeHall;
						if (pHero.getLevel() < 6)
						{//choices  for the fights in the challenge hall
							cout << "You have encountered your enemies choose who be your first opponent:\n";
							cout << "[1] Goblin\n";
							cout << "[2] Skeletons\n";
							cout << "[3] Ghosts \n";
							cin >> ChallengeHall;
						}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						//if player is level 6 
						if (pHero.getLevel() == 6)
						{
							cout << "You are ready " << name << "you have reached this far on your own and are strongehough to progress.\n";
							"Are you sure you're ready to Progress?\n[Y]es or [n]o?";
							cin >> player;//character player

						}
						int battleChoice;
						int theHit;
						int currentHealth;
						system("cls");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						if (pHero.getLevel() == 1)

							cout << "\n> ";
						//validate
						while (!(cin >> ChallengeHall) || ChallengeHall < 1 || ChallengeHall > 3)
						{
							cout << "invalid variable. must be in range of asked choice.\n";
							cin.clear();
						}
						switch (ChallengeHall)
						{
						case 1:
						{
								  cout << "A goblin enters The Stage.\n";
								  character goblin("goblin", 45, 13, 1, 0, 0);
								  return character("fighter", 50, 12, 0.6, 50, 1);//Class(z), Health(a), Str(b), Armor(i), Gold(e), Level(f);
								  while (goblin.getHealth() > 0)
								  {
									  cout << "Which action would you like to take?\n\n[1]Attack\n\n[2]Beg for mercy\n";									  
									  system("cls");

									  cout << "\n> ";
									  //validate
									  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 3)
									  {
										  cout << "invalid variable. must be in range of asked choice.\n";
										  cin.clear();
									  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									  switch (battleChoice)
									  {
									  case 1:
									  {
												//player hero. get str and random % 11 if its greater then goblin(monster). str + random % 11 
												if ((pHero.getStr() + (rand() % 11)) > (goblin.getStr() + (rand() % 11)))
												{
													//if you hit the goblin
													theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
													cout << "You have hit the goblin for " << theHit << "!\n\n";
													currentHealth = goblin.getHealth() - theHit;
													goblin.setHealth(currentHealth);
													if (1 > goblin.getHealth())
													{
														cout << "Goblin Slain you gain 50G and 30xp";
														newGold = pHero.getGold() + 50;//gold gained after the kill
														pHero.setGold(newGold);
														experience = experience + 30;//xperience gained after the kill
														goblin.setHealth(0);
													}
												}
												else
												{
													//if you miss the goblin
													cout << "The goblin has dodged your swing!\n\n";
												}
												if (goblin.getHealth() > 0)
												{
													if ((goblin.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
													{
														theHit = goblin.getStr() * 0.95;
														theHit = theHit * pHero.getArmor();
														cout << "The goblin has hit you for " << theHit;
														currentHealth = pHero.getHealth() - theHit;
														pHero.setHealth(currentHealth);
													}
													else
													{
														cout << "You dodged the goblin";
													}
												}
												system("pause");
												system("cls");
												break;
									  }
									  case 2:
									  {	//if option 2 then its runaway
												//if rand % 3 and less then 2 then run away
												if ((rand() % 3) < 2)
												{
													cout << "You ran away\n";
													system("pause");
													system("cls");
													goblin.setHealth(0);//set the goblins health to 0
													break;
												}
												else
												{
													//if you make the wrong decision in running away then you are dead 
													cout << "Poor decision. The goblin executes you!";
													Sleep(1000);
													system("cls");
													pHero.setHealth(0);//set ur health to 0 and goblins to 0 your dead
													goblin.setHealth(0);
													break;
												}
												break;
									  }
									  default:
										  //default value entered

									  {
												 cout << "Invalid selection.\n\n";
												 system("pause");
												 system("cls");
									  }
									  }
									  if (pHero.getHealth() < 1)
									  {
										  //if your health below 1 then your dead
										  cout << "You have been slain!";
										  Sleep(2000);
										  system("cls");
										  break;
									  }
								  }
								  break;
						}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						case 2:
						{
								  cout << "A skeleton enters The Stage.\n";
								  character skeleton("skeleton", 45, 13, 1, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
								  while (skeleton.getHealth() > 0)//while skeleton has more then 0 health
								  {
									  cout << "[1]Attack\n";
									  cout << "[2]Flee\n";
									  cin >> battleChoice;
									  system("cls");

									  cout << "\n> ";
									  //validate
									  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
									  {
										  cout << "invalid variable. must be in range of asked choice.\n";
										  cin.clear();
									  }
									  switch (battleChoice)
									  {
									  case 1:
									  {
												if ((pHero.getStr() + (rand() % 11)) > (skeleton.getStr() + (rand() % 11)))
												{
													theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
													cout << "You have hit the skeleton for " << theHit;
													currentHealth = skeleton.getHealth() - theHit;
													skeleton.setHealth(currentHealth);
													if (1 > skeleton.getHealth())
													{
														cout << "Skeleton Slain you gain 65G and 50xp\n";
														newGold = pHero.getGold() + 65;
														pHero.setGold(newGold);
														experience = experience + 50;
														skeleton.setHealth(0);
													}
												}
												else
												{
													cout << "You missed\n\n";
												}
												if (skeleton.getHealth() > 0)
												{
													if ((skeleton.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
													{
														theHit = skeleton.getStr()*1.5;
														theHit = theHit*pHero.getArmor();
														cout << "The skeleton has hit you for " << theHit;
														currentHealth = pHero.getHealth() - theHit;
														pHero.setHealth(currentHealth);
													}
													else
													{
														cout << "You have dodged the skeleton";
													}
												}
												system("pause");
												system("cls");
												break;
									  }
									  case 2:
									  {
												if ((rand() % 3) < 2)
												{
													cout << "You fled the field\n";
													system("pause");
													system("cls");
													skeleton.setHealth(0);
													break;
												}
												else
												{
													cout << "You been slain";
													Sleep(2000);
													system("cls");
													pHero.setHealth(0);
													skeleton.setHealth(0);
													break;
												}
												break;
									  }
									  default:
									  {
												 cout << "Invalid selection.\n\n";
												 cout << "Please press enter to continue...\n";
												 system("pause");
												 system("cls");
									  }
									  }
									  if (pHero.getHealth() < 1)
									  {
										  cout << "You have been slain";
										  Sleep(1700);
										  system("cls");
										  break;
									  }
								  }
								  break;
						}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						case 3:
						{
								  cout << "A Ghosts enters The Stage.\n";
								  character Ghosts("skeleton", 45, 13, 1, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
								  while (Ghosts.getHealth() > 0)//while Ghosts has more then 0 health
								  {
									  cout << "[1]Attack\n";
									  cout << "[2]Flee\n";
									  cin >> battleChoice;
									  system("cls");

									  cout << "\n> ";
									  //validate
									  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
									  {
										  cout << "invalid variable. must be in range of asked choice.\n";
										  cin.clear();
									  }
									  switch (battleChoice)
									  {
									  case 1:
									  {
												if ((pHero.getStr() + (rand() % 11)) > (Ghosts.getStr() + (rand() % 11)))
												{
													theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
													cout << "You have hit the Ghosts for " << theHit;
													currentHealth = Ghosts.getHealth() - theHit;
													Ghosts.setHealth(currentHealth);
													if (1 > Ghosts.getHealth())
													{
														cout << "Ghosts Slain you gain 65G and 50xp\n";
														newGold = pHero.getGold() + 65;
														pHero.setGold(newGold);
														experience = experience + 50;
														Ghosts.setHealth(0);
													}
												}
												else
												{
													cout << "You missed\n\n";
												}
												if (Ghosts.getHealth() > 0)
												{
													if ((Ghosts.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
													{
														theHit = Ghosts.getStr()*1.5;
														theHit = theHit * pHero.getArmor();
														cout << "The Ghosts has hit you for " << theHit;
														currentHealth = pHero.getHealth() - theHit;
														pHero.setHealth(currentHealth);
													}
													else
													{
														cout << "You have dodged the skeleton";
													}
												}
												system("pause");
												system("cls");
												break;
									  }
									  case 2:
									  {
												if ((rand() % 3) < 2)
												{
													cout << "You fled the field\n";
													system("pause");
													system("cls");
													Ghosts.setHealth(0);
													break;
												}
												else
												{
													cout << "You been slain";
													Sleep(2000);
													system("cls");
													pHero.setHealth(0);
													Ghosts.setHealth(0);
													break;
												}
												break;
									  }
									  default:
									  {
												 cout << "Invalid selection.\n\n";
												 system("pause");
												 system("cls");
									  }
									  }
									  if (pHero.getHealth() < 1)
									  {
										  cout << "You have been slain";
										  Sleep(1700);
										  system("cls");
										  break;
									  }
								  }
								  break;
						}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							if (pHero.getLevel() == 2)

								cout << "\n> ";
							//validate
							while (!(cin >> ChallengeHall) || ChallengeHall < 1 || ChallengeHall > 3)
							{
								cout << "invalid variable. must be in range of asked choice.\n";
								cin.clear();
							}
							switch (ChallengeHall)
							{
							case 1:
							{
									  cout << "A goblin enters The Stage.\n";
									  character goblin("goblin", 60, 23, 11, 0, 0);
									  while (goblin.getHealth() > 0)
									  {
										  cout << "Which action would you like to take?\n\n[1]Attack\n\n[2]Beg for mercy\n";
										
										  system("cls");

										  cout << "\n> ";
										  //validate
										  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 3)
										  {
											  cout << "invalid variable. must be in range of asked choice.\n";
											  cin.clear();
										  }
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
										  switch (battleChoice)
										  {
										  case 1:
										  {
													//player hero. get str and random % 11 if its greater then goblin(monster). str + random % 11 
													if ((pHero.getStr() + (rand() % 11)) > (goblin.getStr() + (rand() % 11)))
													{
														//if you hit the goblin
														theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
														cout << "You have hit the goblin for " << theHit << "!\n\n";
														currentHealth = goblin.getHealth() - theHit;
														goblin.setHealth(currentHealth);
														if (1 > goblin.getHealth())
														{
															cout << "Goblin Slain you gain 90G and 70xp";
															newGold = pHero.getGold() + 90;//gold gained after the kill
															pHero.setGold(newGold);
															experience = experience + 70;//xperience gained after the kill
															goblin.setHealth(0);
														}
													}
													else
													{
														//if you miss the goblin
														cout << "The goblin has dodged your swing!\n\n";
													}
													if (goblin.getHealth() > 0)
													{
														if ((goblin.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
														{
															theHit = goblin.getStr() * 1.95;
															theHit = theHit * pHero.getArmor();
															cout << "The goblin has hit you for " << theHit;
															currentHealth = pHero.getHealth() - theHit;
															pHero.setHealth(currentHealth);
														}
														else
														{
															cout << "You dodged the goblin";
														}
													}
													system("pause");
													system("cls");
													break;
										  }
										  case 2:
										  {	//if option 2 then its runaway
													//if rand % 3 and less then 2 then run away
													if ((rand() % 3) < 2)
													{
														cout << "You ran away\n";
														system("pause");
														system("cls");
														goblin.setHealth(0);//set the goblins health to 0
														break;
													}
													else
													{
														//if you make the wrong decision in running away then you are dead 
														cout << "Poor decision. The goblin executes you!";
														Sleep(1000);
														system("cls");
														pHero.setHealth(0);//set ur health to 0 and goblins to 0 your dead
														goblin.setHealth(0);
														break;
													}
													break;
										  }
										  default:
											  //default value entered

										  {
													 cout << "Invalid selection.\n\n";
													 system("pause");
													 system("cls");
										  }
										  }
										  if (pHero.getHealth() < 1)
										  {
											  //if your health below 1 then your dead
											  cout << "You have been slain!";
											  Sleep(2000);
											  system("cls");
											  break;
										  }
									  }
									  break;
							}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							case 2:
							{
									  cout << "A skeleton enters The Stage.\n";
									  character skeleton("skeleton", 95, 23, 16, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
									  while (skeleton.getHealth() > 0)//while skeleton has more then 0 health
									  {
										  cout << "[1]Attack\n";
										  cout << "[2]Flee\n";
										  cin >> battleChoice;
										  system("cls");

										  cout << "\n> ";
										  //validate
										  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
										  {
											  cout << "invalid variable. must be in range of asked choice.\n";
											  cin.clear();
										  }
										  switch (battleChoice)
										  {
										  case 1:
										  {
													if ((pHero.getStr() + (rand() % 11)) > (skeleton.getStr() + (rand() % 11)))
													{
														theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
														cout << "You have hit the skeleton for " << theHit;
														currentHealth = skeleton.getHealth() - theHit;
														skeleton.setHealth(currentHealth);
														if (1 > skeleton.getHealth())
														{
															cout << "Skeleton Slain you gain 95G and 60xp\n";
															newGold = pHero.getGold() + 85;
															pHero.setGold(newGold);
															experience = experience + 60;
															skeleton.setHealth(0);
														}
													}
													else
													{
														cout << "You missed\n\n";
													}
													if (skeleton.getHealth() > 0)
													{
														if ((skeleton.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
														{
															theHit = skeleton.getStr() * 1.5;
															theHit = theHit*pHero.getArmor();
															cout << "The skeleton has hit you for " << theHit;
															currentHealth = pHero.getHealth() - theHit;
															pHero.setHealth(currentHealth);
														}
														else
														{
															cout << "You have dodged the skeleton";
														}
													}
													system("pause");
													system("cls");
													break;
										  }
										  case 2:
										  {
													if ((rand() % 3) < 2)
													{
														cout << "You fled the field\n";
														system("pause");
														system("cls");
														skeleton.setHealth(0);
														break;
													}
													else
													{
														cout << "You been slain";
														Sleep(2000);
														system("cls");
														pHero.setHealth(0);
														skeleton.setHealth(0);
														break;
													}
													break;
										  }
										  default:
										  {
													 cout << "Invalid selection.\n\n";
													 cout << "Please press enter to continue...\n";
													 system("pause");
													 system("cls");
										  }
										  }
										  if (pHero.getHealth() < 1)
										  {
											  cout << "You have been slain";
											  Sleep(1700);
											  system("cls");
											  break;
										  }
									  }
									  break;
							}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							case 3:
							{
									  cout << "A Ghosts enters The Stage.\n";
									  character Ghosts("Ghosts", 95, 24, 18, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
									  while (Ghosts.getHealth() > 0)//while Ghosts has more then 0 health
									  {
										  cout << "[1]Attack\n";
										  cout << "[2]Flee\n";
										  system("cls");

										  cout << "\n> ";
										  //validate
										  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
										  {
											  cout << "invalid variable. must be in range of asked choice.\n";
											  cin.clear();
										  }
										  switch (battleChoice)
										  {
										  case 1:
										  {
													if ((pHero.getStr() + (rand() % 11)) > (Ghosts.getStr() + (rand() % 11)))
													{
														theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
														cout << "You have hit the Ghosts for " << theHit;
														currentHealth = Ghosts.getHealth() - theHit;
														Ghosts.setHealth(currentHealth);
														if (1 > Ghosts.getHealth())
														{
															cout << "Ghosts Slain you gain 105G and 98xp\n";
															newGold = pHero.getGold() + 105;
															pHero.setGold(newGold);
															experience = experience + 98;
															Ghosts.setHealth(0);
														}
													}
													else
													{
														cout << "You missed\n\n";
													}
													if (Ghosts.getHealth() > 0)
													{
														if ((Ghosts.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
														{
															theHit = Ghosts.getStr() * 1.5;
															theHit = theHit * pHero.getArmor();
															cout << "The Ghosts has hit you for " << theHit;
															currentHealth = pHero.getHealth() - theHit;
															pHero.setHealth(currentHealth);
														}
														else
														{
															cout << "You have dodged the Ghosts";
														}
													}
													system("pause");
													system("cls");
													break;
										  }
										  case 2:
										  {
													if ((rand() % 3) < 2)
													{
														cout << "You fled the field\n";
														system("pause");
														system("cls");
														Ghosts.setHealth(0);
														break;
													}
													else
													{
														cout << "You been slain";
														Sleep(2000);
														system("cls");
														pHero.setHealth(0);
														Ghosts.setHealth(0);
														break;
													}
													break;
										  }
										  default:
										  {
													 cout << "Invalid selection.\n\n";
													 system("pause");
													 system("cls");
										  }
										  }
										  if (pHero.getHealth() < 1)
										  {
											  cout << "You have been slain";
											  Sleep(1700);
											  system("cls");
											  break;
										  }
									  }
									  break;
							}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								if (pHero.getLevel() == 3)

									cout << "\n> ";
								//validate
								while (!(cin >> ChallengeHall) || ChallengeHall < 1 || ChallengeHall > 3)
								{
									cout << "invalid variable. must be in range of asked choice.\n";
									cin.clear();
								}
								switch (ChallengeHall)
								{
								case 1:
								{
										  cout << "A goblin enters The Stage.\n";
										  character goblin("goblin", 160, 43, 31, 0, 0);
										  while (goblin.getHealth() > 0)
										  {
											  cout << "Which action would you like to take?\n\n[1]Attack\n\n[2]Beg for mercy\n";
											
											  system("cls");

											  cout << "\n> ";
											  //validate
											  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 3)
											  {
												  cout << "invalid variable. must be in range of asked choice.\n";
												  cin.clear();
											  }
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		switch (battleChoice)
		{
		case 1:
		{
				  //player hero. get str and random % 11 if its greater then goblin(monster). str + random % 11 
				  if ((pHero.getStr() + (rand() % 11)) > (goblin.getStr() + (rand() % 11)))
				  {
					  //if you hit the goblin
					  theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
					  cout << "You have hit the goblin for " << theHit << "!\n\n";
					  currentHealth = goblin.getHealth() - theHit;
					  goblin.setHealth(currentHealth);
					  if (1 > goblin.getHealth())
					  {
						  cout << "Goblin Slain you gain 190G and 170xp";
						  newGold = pHero.getGold() + 190;//gold gained after the kill
						  pHero.setGold(newGold);
						  experience = experience + 170;//xperience gained after the kill
						  goblin.setHealth(0);
					  }
				  }
				  else
				  {
					  //if you miss the goblin
					  cout << "The goblin has dodged your swing!\n\n";
				  }
				  if (goblin.getHealth() > 0)
				  {
					  if ((goblin.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
					  {
						  theHit = goblin.getStr() * 2.95;
						  theHit = theHit * pHero.getArmor();
						  cout << "The goblin has hit you for " << theHit;
						  currentHealth = pHero.getHealth() - theHit;
						  pHero.setHealth(currentHealth);
					  }
					  else
					  {
						  cout << "You dodged the goblin";
					  }
				  }
				  system("pause");
				  system("cls");
				  break;
		}
		case 2:
		{	//if option 2 then its runaway
				  //if rand % 3 and less then 2 then run away
				  if ((rand() % 3) < 2)
				  {
					  cout << "You ran away\n";
					  system("pause");
					  system("cls");
					  goblin.setHealth(0);//set the goblins health to 0
					  break;
				  }
				  else
				  {
					  //if you make the wrong decision in running away then you are dead 
					  cout << "Poor decision. The goblin executes you!";
					  Sleep(1000);
					  system("cls");
					  pHero.setHealth(0);//set ur health to 0 and goblins to 0 your dead
					  goblin.setHealth(0);
					  break;
				  }
				  break;				  
		}
		default:
			//default value entered
			
		{
				   cout << "Invalid selection.\n\n";
				   
				   system("pause");
				   system("cls");
		}
		}
		if (pHero.getHealth() < 1)
		{
			//if your health below 1 then your dead
			cout << "You have been slain!";
			Sleep(2000);
			system("cls");
			break;
		}
										  }
break;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	case 2:
	{
			  cout << "A skeleton enters The Stage.\n";
			  character skeleton("skeleton", 195, 33, 26, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
			  while (skeleton.getHealth() > 0)//while skeleton has more then 0 health
			  {
				  cout << "[1]Attack\n";
				  cout << "[2]Flee\n";
				  cin >> battleChoice;
				  system("cls");
				  
				  cout << "\n> ";
				  //validate
				  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
				  {
					  cout << "invalid variable. must be in range of asked choice.\n";
					  cin.clear();
				  }
				  switch (battleChoice)
				  {
				  case 1:
				  {
							if ((pHero.getStr() + (rand() % 11)) > (skeleton.getStr() + (rand() % 11)))
							{
								theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
								cout << "You have hit the skeleton for " << theHit;
								currentHealth = skeleton.getHealth() - theHit;
								skeleton.setHealth(currentHealth);								
								if (1 > skeleton.getHealth())
								{
									cout << "Skeleton Slain you gain 185G and 60xp\n";
									newGold = pHero.getGold() + 185;
									pHero.setGold(newGold);
									experience = experience + 160;
									skeleton.setHealth(0);
								}
							}
							else
							{
								cout << "You missed\n\n";
							}
							if (skeleton.getHealth() > 0)
							{
								if ((skeleton.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
								{
									theHit = skeleton.getStr() * 2.5;
									theHit = theHit*pHero.getArmor();
									cout << "The skeleton has hit you for " << theHit;
									currentHealth = pHero.getHealth() - theHit;
									pHero.setHealth(currentHealth);
								}
								else
								{
									cout << "You have dodged the skeleton";
								}
							}
							system("pause");
							system("cls");
							break;
				  }
				  case 2:					  
				  {
							if ((rand() % 3) < 2)
							{
								cout << "You fled the field\n";
								system("pause");
								system("cls");
								skeleton.setHealth(0);
								break;
							}
							else
							{
								cout << "You been slain";
								Sleep(2000);
								system("cls");
								pHero.setHealth(0);
								skeleton.setHealth(0);
								break;
							}
							break;
				  }
				  default:
				  {
							 cout << "Invalid selection.\n\n";
							 cout << "Please press enter to continue...\n";
							 system("pause");
							 system("cls");							 
				  }
				  }
				  if (pHero.getHealth() < 1)
				  {
					  cout << "You have been slain";
					  Sleep(1700);
					  system("cls");
					  break;
				  }
			  }
			  break;
	}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			case 3:
			{
					  cout << "A Ghosts enters The Stage.\n";
					  character Ghosts("Ghosts", 195, 64, 58, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
					  while (Ghosts.getHealth() > 0)//while Ghosts has more then 0 health
					  {
						  cout << "[1]Attack\n";
						  cout << "[2]Flee\n";
						  cin >> battleChoice;
						  system("cls");
						  cout << "\n> ";
						  //validate
						  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
						  {
							  cout << "invalid variable. must be in range of asked choice.\n";
							  cin.clear();
						  }
						  switch (battleChoice)
						  {
						  case 1:
						  {
									if ((pHero.getStr() + (rand() % 11)) > (Ghosts.getStr() + (rand() % 11)))
									{
										theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
										cout << "You have hit the Ghosts for " << theHit;
										currentHealth = Ghosts.getHealth() - theHit;
										Ghosts.setHealth(currentHealth);
										if (1 > Ghosts.getHealth())
										{
											cout << "Ghosts Slain you gain 205G and 198xp\n";
											newGold = pHero.getGold() + 205;
											pHero.setGold(newGold);
											experience = experience + 198;
											Ghosts.setHealth(0);
										}
									}
									else
									{
										cout << "You missed\n\n";
									}
									if (Ghosts.getHealth()>0)
									{
										if ((Ghosts.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
										{
											theHit = Ghosts.getStr() * 2.5;
											theHit = theHit * pHero.getArmor();
											cout << "The Ghosts has hit you for " << theHit;
											currentHealth = pHero.getHealth() - theHit;
											pHero.setHealth(currentHealth);
										}
										else
										{
											cout << "You have dodged the skeleton";
										}
									}
									system("pause");
									system("cls");
									break;
						  }
						  case 2:
						  {
									if ((rand() % 3) < 2)
									{
										cout << "You fled the field\n";
										system("pause");
										system("cls");
										Ghosts.setHealth(0);
										break;
									}
									else
									{
										cout << "You been slain";
										Sleep(2000);
										system("cls");
										pHero.setHealth(0);
										Ghosts.setHealth(0);
										break;
									}
									break;
						  }
						  default:
						  {
									 cout << "Invalid selection.\n\n";
									 cout << "Please press enter to continue...\n";
									 system("pause");
									 system("cls");
						  }
						  }
						  if (pHero.getHealth() < 1)
						  {
							  cout << "You have been slain";
							  Sleep(1700);
							  system("cls");
							  break;
						  }
					  }
					  break;
			}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
									if (pHero.getLevel() == 4)

										cout << "\n> ";
									//validate
									while (!(cin >> ChallengeHall) || ChallengeHall < 1 || ChallengeHall > 3)
									{
										cout << "invalid variable. must be in range of asked choice.\n";
										cin.clear();
									}
									switch (ChallengeHall)
									{
									case 1:
									{
											  cout << "A goblin enters The Stage.\n";
											  character goblin("goblin", 260, 143, 41, 0, 0);
											  while (goblin.getHealth() > 0)
											  {
												  cout << "Which action would you like to take?\n\n[1]Attack\n\n[2]Beg for mercy\n";
												  
												  system("cls");

												  cout << "\n> ";
												  //validate
												  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 3)
												  {
													  cout << "invalid variable. must be in range of asked choice.\n";
													  cin.clear();
												  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
												  switch (battleChoice)
												  {
												  case 1:
												  {
															//player hero. get str and random % 11 if its greater then goblin(monster). str + random % 11 
															if ((pHero.getStr() + (rand() % 11)) > (goblin.getStr() + (rand() % 11)))
															{
																//if you hit the goblin
																theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																cout << "You have hit the goblin for " << theHit << "!\n\n";
																currentHealth = goblin.getHealth() - theHit;
																goblin.setHealth(currentHealth);
																if (1 > goblin.getHealth())
																{
																	cout << "Goblin Slain you gain 1190G and 1170xp";
																	newGold = pHero.getGold() + 1190;//gold gained after the kill
																	pHero.setGold(newGold);
																	experience = experience + 1170;//xperience gained after the kill
																	goblin.setHealth(0);
																}
															}
															else
															{
																//if you miss the goblin
																cout << "The goblin has dodged your swing!\n\n";
															}
															if (goblin.getHealth() > 0)
															{
																if ((goblin.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																{
																	theHit = goblin.getStr() * 3.95;
																	theHit = theHit * pHero.getArmor();
																	cout << "The goblin has hit you for " << theHit;
																	currentHealth = pHero.getHealth() - theHit;
																	pHero.setHealth(currentHealth);
																}
																else
																{
																	cout << "You dodged the goblin";
																}
															}
															system("pause");
															system("cls");
															break;
												  }
												  case 2:
												  {	//if option 2 then its runaway
															//if rand % 3 and less then 2 then run away
															if ((rand() % 3) < 2)
															{
																cout << "You ran away\n";
																system("pause");
																system("cls");
																goblin.setHealth(0);//set the goblins health to 0
																break;
															}
															else
															{
																//if you make the wrong decision in running away then you are dead 
																cout << "Poor decision. The goblin executes you!";
																Sleep(1000);
																system("cls");
																pHero.setHealth(0);//set ur health to 0 and goblins to 0 your dead
																goblin.setHealth(0);
																break;
															}
															break;
												  }
												  default:
													  //default value entered

												  {
															 cout << "Invalid selection.\n\n";
															 system("pause");
															 system("cls");
												  }
												  }
												  if (pHero.getHealth() < 1)
												  {
													  //if your health below 1 then your dead
													  cout << "You have been slain!";
													  Sleep(2000);
													  system("cls");
													  break;
												  }
											  }
											  break;
									}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

									case 2:
									{
											  cout << "A skeleton enters The Stage.\n";
											  character skeleton("skeleton", 295, 143, 46, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
											  while (skeleton.getHealth() > 0)//while skeleton has more then 0 health
											  {
												  cout << "[1]Attack\n";
												  cout << "[2]Flee\n";
												  cin >> battleChoice;
												  system("cls");

												  cout << "\n> ";
												  //validate
												  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
												  {
													  cout << "invalid variable. must be in range of asked choice.\n";
													  cin.clear();
												  }
												  switch (battleChoice)
												  {
												  case 1:
												  {
															if ((pHero.getStr() + (rand() % 11)) > (skeleton.getStr() + (rand() % 11)))
															{
																theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																cout << "You have hit the skeleton for " << theHit;
																currentHealth = skeleton.getHealth() - theHit;
																skeleton.setHealth(currentHealth);
																if (1 > skeleton.getHealth())
																{
																	cout << "Skeleton Slain you gain 1185G and 1160xp\n";
																	newGold = pHero.getGold() + 1185;
																	pHero.setGold(newGold);
																	experience = experience + 1160;
																	skeleton.setHealth(0);
																}
															}
															else
															{
																cout << "You missed\n\n";
															}
															if (skeleton.getHealth() > 0)
															{
																if ((skeleton.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																{
																	theHit = skeleton.getStr() * 4.5;
																	theHit = theHit*pHero.getArmor();
																	cout << "The skeleton has hit you for " << theHit;
																	currentHealth = pHero.getHealth() - theHit;
																	pHero.setHealth(currentHealth);
																}
																else
																{
																	cout << "You have dodged the skeleton";
																}
															}
															system("pause");
															system("cls");
															break;
												  }
												  case 2:
												  {
															if ((rand() % 3) < 2)
															{
																cout << "You fled the field\n";
																system("pause");
																system("cls");
																skeleton.setHealth(0);
																break;
															}
															else
															{
																cout << "You been slain";
																Sleep(2000);
																system("cls");
																pHero.setHealth(0);
																skeleton.setHealth(0);
																break;
															}
															break;
												  }
												  default:
												  {
															 cout << "Invalid selection.\n\n";
															 cout << "Please press enter to continue...\n";
															 system("pause");
															 system("cls");
												  }
												  }
												  if (pHero.getHealth() < 1)
												  {
													  cout << "You have been slain";
													  Sleep(1700);
													  system("cls");
													  break;
												  }
											  }
											  break;
									}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

									case 3:
									{
											  cout << "A Ghosts enters The Stage.\n";
											  character Ghosts("Ghosts", 595, 164, 48, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
											  while (Ghosts.getHealth() > 0)//while Ghosts has more then 0 health
											  {
												  cout << "[1]Attack\n";
												  cout << "[2]Flee\n";
												  system("cls");

												  cout << "\n> ";
												  //validate
												  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
												  {
													  cout << "invalid variable. must be in range of asked choice.\n";
													  cin.clear();
												  }
												  switch (battleChoice)
												  {
												  case 1:
												  {
															if ((pHero.getStr() + (rand() % 11)) > (Ghosts.getStr() + (rand() % 11)))
															{
																theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																cout << "You have hit the Ghosts for " << theHit;
																currentHealth = Ghosts.getHealth() - theHit;
																Ghosts.setHealth(currentHealth);
																if (1 > Ghosts.getHealth())
																{
																	cout << "Ghosts Slain you gain 1205G and 1198xp\n";
																	newGold = pHero.getGold() + 1205;
																	pHero.setGold(newGold);
																	experience = experience + 1198;
																	Ghosts.setHealth(0);
																}
															}
															else
															{
																cout << "You missed\n\n";
															}
															if (Ghosts.getHealth() > 0)
															{
																if ((Ghosts.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																{
																	theHit = Ghosts.getStr() * 4.5;
																	theHit = theHit * pHero.getArmor();
																	cout << "The Ghosts has hit you for " << theHit;
																	currentHealth = pHero.getHealth() - theHit;
																	pHero.setHealth(currentHealth);
																}
																else
																{
																	cout << "You have dodged the skeleton";
																}
															}
															system("pause");
															system("cls");
															break;
												  }
												  case 2:
												  {
															if ((rand() % 3) < 2)
															{
																cout << "You fled the field\n";
																system("pause");
																system("cls");
																Ghosts.setHealth(0);
																break;
															}
															else
															{
																cout << "You been slain";
																Sleep(2000);
																system("cls");
																pHero.setHealth(0);
																Ghosts.setHealth(0);
																break;
															}
															break;
												  }
												  default:
												  {
															 cout << "Invalid selection.\n\n";
															 system("pause");
															 system("cls");
												  }
												  }
												  if (pHero.getHealth() < 1)
												  {
													  cout << "You have been slain";
													  Sleep(1700);
													  system("cls");
													  break;
												  }
											  }
											  break;
									}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
										if (pHero.getLevel() == 5)

											cout << "\n> ";
										//validate
										while (!(cin >> ChallengeHall) || ChallengeHall < 1 || ChallengeHall > 3)
										{
											cout << "invalid variable. must be in range of asked choice.\n";
											cin.clear();
										}
										switch (ChallengeHall)
										{
										case 1:
										{
												  cout << "A goblin enters The Stage.\n";
												  character goblin("goblin", 360, 243, 51, 0, 0);
												  while (goblin.getHealth() > 0)
												  {
													  cout << "Which action would you like to take?\n\n[1]Attack\n\n[2]Beg for mercy\n";
												
													  system("cls");

													  cout << "\n> ";
													  //validate
													  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 3)
													  {
														  cout << "invalid variable. must be in range of asked choice.\n";
														  cin.clear();
													  }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
													  switch (battleChoice)
													  {
													  case 1:
													  {
																//player hero. get str and random % 11 if its greater then goblin(monster). str + random % 11 
																if ((pHero.getStr() + (rand() % 11)) > (goblin.getStr() + (rand() % 11)))
																{
																	//if you hit the goblin
																	theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																	cout << "You have hit the goblin for " << theHit << "!\n\n";
																	currentHealth = goblin.getHealth() - theHit;
																	goblin.setHealth(currentHealth);
																	if (1 > goblin.getHealth())
																	{
																		cout << "Goblin Slain you gain 2190G and 2170xp";
																		newGold = pHero.getGold() + 2190;//gold gained after the kill
																		pHero.setGold(newGold);
																		experience = experience + 2170;//xperience gained after the kill
																		goblin.setHealth(0);
																	}
																}
																else
																{
																	//if you miss the goblin
																	cout << "The goblin has dodged your swing!\n\n";
																}
																if (goblin.getHealth() > 0)
																{
																	if ((goblin.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																	{
																		theHit = goblin.getStr() * 5.95;
																		theHit = theHit * pHero.getArmor();
																		cout << "The goblin has hit you for " << theHit;
																		currentHealth = pHero.getHealth() - theHit;
																		pHero.setHealth(currentHealth);
																	}
																	else
																	{
																		cout << "You dodged the goblin";
																	}
																}
																system("pause");
																system("cls");
																break;
													  }
													  case 2:
													  {	//if option 2 then its runaway
																//if rand % 3 and less then 2 then run away
																if ((rand() % 3) < 2)
																{
																	cout << "You ran away\n";
																	system("pause");
																	system("cls");
																	goblin.setHealth(0);//set the goblins health to 0
																	break;
																}
																else
																{
																	//if you make the wrong decision in running away then you are dead 
																	cout << "Poor decision. The goblin executes you!";
																	Sleep(1000);
																	system("cls");
																	pHero.setHealth(0);//set ur health to 0 and goblins to 0 your dead
																	goblin.setHealth(0);
																	break;
																}
																break;
													  }
													  default:
														  //default value entered

													  {
																 cout << "Invalid selection.\n\n";
																 system("pause");
																 system("cls");
													  }
													  }
													  if (pHero.getHealth() < 1)
													  {
														  //if your health below 1 then your dead
														  cout << "You have been slain!";
														  Sleep(2000);
														  system("cls");
														  break;
													  }
												  }
												  break;
										}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

										case 2:
										{
												  cout << "A skeleton enters The Stage.\n";
												  character skeleton("skeleton", 295, 243, 56, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
												  while (skeleton.getHealth() > 0)//while skeleton has more then 0 health
												  {
													  cout << "[1]Attack\n";
													  cout << "[2]Flee\n";
													  cin >> battleChoice;
													  system("cls");

													  cout << "\n> ";
													  //validate
													  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
													  {
														  cout << "invalid variable. must be in range of asked choice.\n";
														  cin.clear();
													  }
													  switch (battleChoice)
													  {
													  case 1:
													  {
																if ((pHero.getStr() + (rand() % 11)) > (skeleton.getStr() + (rand() % 11)))
																{
																	theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																	cout << "You have hit the skeleton for " << theHit;
																	currentHealth = skeleton.getHealth() - theHit;
																	skeleton.setHealth(currentHealth);
																	if (1 > skeleton.getHealth())
																	{
																		cout << "Skeleton Slain you gain 2185G and 2160xp\n";
																		newGold = pHero.getGold() + 2185;
																		pHero.setGold(newGold);
																		experience = experience + 2160;
																		skeleton.setHealth(0);
																	}
																}
																else
																{
																	cout << "You missed\n\n";
																}
																if (skeleton.getHealth() > 0)
																{
																	if ((skeleton.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																	{
																		theHit = skeleton.getStr() * 5.5;
																		theHit = theHit*pHero.getArmor();
																		cout << "The skeleton has hit you for " << theHit;
																		currentHealth = pHero.getHealth() - theHit;
																		pHero.setHealth(currentHealth);
																	}
																	else
																	{
																		cout << "You have dodged the skeleton";
																	}
																}
																system("pause");
																system("cls");
																break;
													  }
													  case 2:
													  {
																if ((rand() % 3) < 2)
																{
																	cout << "You fled the field\n";
																	system("pause");
																	system("cls");
																	skeleton.setHealth(0);
																	break;
																}
																else
																{
																	cout << "You been slain";
																	Sleep(2000);
																	system("cls");
																	pHero.setHealth(0);
																	skeleton.setHealth(0);
																	break;
																}
																break;
													  }
													  default:
													  {
																 cout << "Invalid selection.\n\n";
																 cout << "Please press enter to continue...\n";
																 system("pause");
																 system("cls");
													  }
													  }
													  if (pHero.getHealth() < 1)
													  {
														  cout << "You have been slain";
														  Sleep(1700);
														  system("cls");
														  break;
													  }
												  }
												  break;
										}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

										case 3:
										{
												  cout << "A Ghosts enters The Stage.\n";
												  character Ghosts("Ghosts", 595, 464, 68, 0, 0);//Class(z), Health(a), Str(b),	Armor(i), Gold(e), Level(f);
												  while (Ghosts.getHealth() > 0)//while Ghosts has more then 0 health
												  {
													  cout << "[1]Attack\n";
													  cout << "[2]Flee\n";
													  system("cls");

													  cout << "\n> ";
													  //validate
													  while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
													  {
														  cout << "invalid variable. must be in range of asked choice.\n";
														  cin.clear();
													  }
													  switch (battleChoice)
													  {
													  case 1:
													  {
																if ((pHero.getStr() + (rand() % 11)) > (Ghosts.getStr() + (rand() % 11)))
																{
																	theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
																	cout << "You have hit the Ghosts for " << theHit;
																	currentHealth = Ghosts.getHealth() - theHit;
																	Ghosts.setHealth(currentHealth);
																	if (1 > Ghosts.getHealth())
																	{
																		cout << "Ghosts Slain you gain 2205G and 2198xp\n";
																		newGold = pHero.getGold() + 2205;
																		pHero.setGold(newGold);
																		experience = experience + 2198;
																		Ghosts.setHealth(0);
																	}
																}
																else
																{
																	cout << "You missed\n\n";
																}
																if (Ghosts.getHealth() > 0)
																{
																	if ((Ghosts.getStr() + (rand() % 11)) > (pHero.getStr() + (rand() % 11)))
																	{
																		theHit = Ghosts.getStr() * 6.5;
																		theHit = theHit * pHero.getArmor();
																		cout << "The Ghosts has hit you for " << theHit;
																		currentHealth = pHero.getHealth() - theHit;
																		pHero.setHealth(currentHealth);
																	}
																	else
																	{
																		cout << "You have dodged the skeleton";
																	}
																}
																system("pause");
																system("cls");
																break;
													  }
													  case 2:
													  {
																if ((rand() % 3) < 2)
																{
																	cout << "You fled the field\n";
																	system("pause");
																	system("cls");
																	Ghosts.setHealth(0);
																	break;
																}
																else
																{
																	cout << "You been slain";
																	Sleep(2000);
																	system("cls");
																	pHero.setHealth(0);
																	Ghosts.setHealth(0);
																	break;
																}
																break;
													  }
													  default:
													  {
																 cout << "Invalid selection.\n\n";
																 system("pause");
																 system("cls");
													  }
													  }
													  if (pHero.getHealth() < 1)
													  {
														  cout << "You have been slain";
														  Sleep(1700);
														  system("cls");
														  break;
													  }
												  }
												  break;
										}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (pHero.getLevel() == 6)
switch (player)
{
case 'y':
{
			cout << "Unknown person appears";
			character person("???person", 600, 80, 1, 0, 0);

			while (person.getHealth()>0)
			{
				cout << "[1]Attack\n";
				cout << "[2]Flee\n";
				system("cls");
				while (!(cin >> battleChoice) || battleChoice < 1 || battleChoice > 2)
				{
					cout << "invalid variable. must be in range of asked choice.\n";
					cin.clear();
				}
				switch (battleChoice)
				{
				case 1:
				{
						  if ((pHero.getStr() + (rand() % 11))>(person.getStr() + (rand() % 11)))
						  {
							  theHit = pHero.getStr()*(pWeapon.getDamageMultiplier1() + pWeapon.getDamageMultiplier2());
							  cout << "You have landed a hit " << theHit;
							  currentHealth = person.getHealth() - theHit;
							  person.setHealth(currentHealth);
							  if (1 > person.getHealth())
							  {
								  cout << "You have defeated ???person";
								  system("cls");
								  cout << "********************************************************************************\n";
								  cout << "******************                End Of                   *********************\n";
								  cout << "******************             Chapter Two                 *********************\n";
								  cout << "********************************************************************************\n";
								  cin.ignore();
								  cin.get();
								  pHero.setHealth(0);

							  }
						  }
						  else
						  {
							  cout << "???person has the upper hand\n\n";
						  }
						  if (person.getHealth()>0)
						  {
							  if ((person.getStr() + (rand() % 11))>(pHero.getStr() + (rand() % 11)))
							  {
								  theHit = person.getStr()*3.3;
								  theHit = theHit*pHero.getArmor();
								  cout << "The personhits you for " << theHit;
								  currentHealth = pHero.getHealth() - theHit;
								  pHero.setHealth(currentHealth);
							  }
							  else
							  {
								  cout << "You  Dodged him";
							  }
						  }
						  cin.ignore();
						  cin.get();
						  system("cls");
						  break;
				}
				case 2:
				{
						  cout << "You died\n";
						  Sleep(2000);
						  system("cls");
						  pHero.setHealth(0);
						  person.setHealth(0);
						  break;
				}
				default:
				{
						   cout << "Invalid selection.\n\n";
						   system("pause");
						   system("cls");
				}
				}
				if (pHero.getHealth()<1)
				{
					cout << "You have been slain";
					Sleep(1700);
					system("cls");
					break;
				}
			}
			break;
}
default:
{
		   system("cls");
		   break;
}
}
break;
			}
//_____________________________________________________________________________________________________________________
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//_____________________________________________________________________________________________________________________
case 3:     // Item Store
{
				system("cls");//system clear
				cout << "Welcome to The Items Store";
				cout << "Gold:  " << pHero.getGold();//display gold
				cout << "What would you like to buy?\n";
				cout << "[1]Health - 25g\n";
				cout << "[2]HI- Health- 45g\n";
				cout << "[3]Greater Health - 100g\n";
				cout << "[4]Potion of Strength - 65g\n";
				cout << "[5]Exit\n";
				int choice;

				system("cls");
				cout << "\n> ";
				//validate
				while (!(cin >> choice) || choice < 1 || choice > 2)
				{
					cout << "invalid variable. must be in range of asked choice.\n";

				switch (choice)
				{
				case 1:
				{
						  if (pHero.getGold()>24)
						  {//use health
							  newHealth = pHero.getHealth() + 20;
							  pHero.setHealth(newHealth);
							  newGold = pHero.getGold() - 25;
							  pHero.setGold(newGold);
							  cout << "You feel bit refresh";
							  system("pause");
							  system("cls");
							  break;
						  }
						  else
						  {
							  cout << "Your out of gold";
							  system("pause");
							  system("cls");
							  break;
						  }
				}
				case 2:
				{//use hi-health
						  if (pHero.getGold()>44)
						  {
							  newHealth = pHero.getHealth() + 45;
							  pHero.setHealth(newHealth);
							  newGold = pHero.getGold() - 45;
							  pHero.setGold(newGold);
							  cout << "You feel bit refresh";
							  system("pause");
							  system("cls");
							  break;
						  }
						  else
						  {
							  cout << "Your out of gold";
							  system("pause");
							  system("cls");
							  break;
						  }
				}
				case 3:
				{//greater health
						  if (pHero.getGold()>99)
						  {
							  newHealth = pHero.getHealth() + 110;
							  pHero.setHealth(newHealth);
							  newGold = pHero.getGold() - 100;
							  pHero.setGold(newGold);
							  cout << "You feel bit refresh";
							  system("pause");
							  system("cls");
							  break;
						  }
						  else
						  {
							  cout << "Your out of gold";
							  system("pause");
							  system("cls");
							  break;
						  }
				}
				case 4:
				{
						  //gain strength
						  if (pHero.getGold()>64)
						  {
							  newStr = pHero.getStr() + 1;
							  pHero.setStr(newStr);
							  newGold = pHero.getGold() - 65;
							  pHero.setGold(newGold);
							  cout << "You feel bit Stronger";
							  system("pause");
							  system("cls");
							  break;
						  }
						  else
						  {
							  cout << "Your out of gold";
							  cin.ignore();
							  cin.get();
							  system("cls");
							  break;
						  }
				}
				
				case 5:
				{
						  //esit
						  break;	
				}
				default:
				{
						   cout << "Invalid selection.\n\n";
						   system("pause");
						   system("cls");
				}
			}
			break;			
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 case 4:     // Stats
 {
				 cout << "You stats \n\nClass: " << pHero.getClass() << ", Name: " << name << "." <<endl;
				 cout << "Health: " << pHero.getHealth() << "Strength: " << pHero.getStr() << endl;
				 cout << "Weapon stats for " << pWeapon.getWeaponName() << "." << endl;
				 cout << "Upgrade: " << pWeapon.getDamageMultiplier1() << endl;
				 cout << "Attachment: " << pWeapon.getDamageMultiplier2() << endl;
				 cin.ignore();
				 cin.get();
				 system("cls");
				 break;
 }
	 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 return 0;
}