//getting the getters and setters for the character classes
#include <iostream>//Standard Input / Output Streams Library
#include <string>
#include "Character.h"//character  header file

using namespace std;


character::character(string z, int a, int b, float i, int e, int f)
{
	//setter for the character class
	setClass(z);
	setHealth(a);
	setStr(b);
	setArmor(i);
	setGold(e);
	setLevel(f);
}
//setting the setteres
void character::setClass(string x)
{
	pClass = x;
}
void character::setHealth(int h)
{
	health = h;
}
void character::setStr(int s)
{
	str = s;
}
void character::setArmor(float arm)
{
	armor = arm;
}
void character::setGold(int g)
{
	gold = g;
}
void character::setLevel(int l)
{
	level = l;
}
void character::addHealth()
{
	++health;
}
void character::subHealth()//when attacked the health will loose
{
	--health;
}
void character::addGold()
{
	++gold;
}
void character::subGold()//apon spending the gold you will need to substract them
{
	--gold;
}
void character::addLevel()//adding level to level up
{
	++level;
}
